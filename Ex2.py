if __name__=="__main__":
    print("Hello World")
    
    myname="Alejandro"
    age=21
    birthday="26/01/1999"
    #Help for method 1
    #https://stackoverflow.com/questions/15286401/print-multiple-arguments-in-python
    print("Print using Method 1:")
    print("My name is %s and I'm %d years old, I was born the %s \n" %(myname,age,birthday))

    #Help for method 2
    #https://www.geeksforgeeks.org/formatted-string-literals-f-strings-python/
    print("Print using Method 2:")
    print(f"My name is {myname} and I'm {age} years old, I was born the {birthday}")