if __name__=="__main__":
    #https://www.tutorialspoint.com/python/python_basic_operators.htm


    #Plus -> Addition
    print (f"5+3 = {5+3}")
    #Minus -> Subtraction
    print (f"5-3 = {5-3}")
    #Slash-> Division
    print (f"5/3 = {5/3}")
    #Double-slash -> Floor Division
    print (f"5//3 = {5//3}")
    #Asterik-> Multiplication
    print (f"5*3 = {5*3}")
    #Less-than
    print (f"5<3 = {5<3}")
    #Greater-than
    print (f"5>3 = {5>3}")

    #Less-than-equal
    print (f"5<=5 = {5<=3}")
    #Greater-than-equal
    print (f"5>=3 = {5>=3}")
    #Equals
    print (f"5==3 = {5==3}")